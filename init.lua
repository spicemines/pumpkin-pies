-- Pumpkin Puree
minetest.register_craftitem("pumpkin_pies:pumpkin_puree", {
    description = "Pumpkin Puree",
    inventory_image = "pumpkin_puree.png",
    on_use = minetest.item_eat(2)
})

minetest.register_craft({
    type = "shapeless",
    output = "pumpkin_pies:pumpkin_puree",
    recipe = {"farming:pumpkin_slice", "farming:mortar_pestle"},
    replacements = {{"farming:mortar_pestle", "farming:mortar_pestle"}}
})

-- Basic Pumpkin Pie
minetest.register_craftitem("pumpkin_pies:pumpkin_pie", {
    description = "Pumpkin Pie",
    inventory_image = "pumpkin_pie_basic.png",
    on_use = minetest.item_eat(6)
})

minetest.register_craft({
    output = "pumpkin_pies:pumpkin_pie",
    recipe = {
        {"farming:baking_tray", "group:food_sugar", ""},
        {"pumpkin_pies:pumpkin_puree", "pumpkin_pies:pumpkin_puree", "pumpkin_pies:pumpkin_puree"},
        {"group:food_wheat", "group:food_wheat", "group:food_wheat"}
    },
    replacements = {{"group:food_baking_tray", "farming:baking_tray"}}
})

-- Cooked Pumpkin Pie
minetest.register_craftitem("pumpkin_pies:pumpkin_pie_cooked", {
    description = "Cooked Pumpkin Pie",
    inventory_image = "pumpkin_pie_cooked.png",
    on_use = minetest.item_eat(8)
})

minetest.register_craft({
    type = "cooking",
    output = "pumpkin_pies:pumpkin_pie_cooked",
    recipe = "pumpkin_pies:pumpkin_pie",
    cooktime = 10
})

-- Applesause as an egg alternative
minetest.register_craftitem("pumpkin_pies:applesauce", {
    description = "Applesauce",
    inventory_image = "applesauce.png",
    on_use = minetest.item_eat(2, "vessels:drinking_glass"),
    groups = {vessel = 1, compostability = 65}
})

minetest.register_craft ({
    output = "pumpkin_pies:applesauce",
    recipe = {
        {"default:apple"},
        {"farming:mortar_pestle"},
        {"vessels:drinking_glass"}
    },
    replacements = {
        {"farming:mortar_pestle", "farming:mortar_pestle"},
    }
})

-- Pumpkin Pie Mix
minetest.register_craftitem("pumpkin_pies:pumpkin_pie_mix", {
    description = "Pumpkin Pie Mix",
    inventory_image = "pumpkin_pie_mix.png",
    on_use = minetest.item_eat(6)
})

minetest.register_craft({
    output = "pumpkin_pies:pumpkin_pie_mix",
    recipe = {
        {"group:food_egg", "group:food_sugar", "group:food_milk"},
        {"pumpkin_pies:pumpkin_puree", "pumpkin_pies:pumpkin_puree", "pumpkin_pies:pumpkin_puree"},
        {"pumpkin_pies:pumpkin_puree", "group:food_mixing_bowl", "pumpkin_pies:pumpkin_puree"}
    },
    replacements = {
        {"group:food_mixing_bowl", "farming:mixing_bowl"},
        {"mobs:bucket_milk", "bucket:bucket_empty"},
        {"mobs:wooden_bucket_milk", "wooden_bucket:wooden_bucket_empty"},
        {"animalia:bucket_milk", "bucket:bucket_empty"}
    }
})

-- Pumpkin Pie Mix Alternative 1
minetest.register_craftitem("pumpkin_pies:pumpkin_pie_mix", {
    description = "Pumpkin Pie Mix",
    inventory_image = "pumpkin_pie_mix.png",
    on_use = minetest.item_eat(6)
})

minetest.register_craft({
    output = "pumpkin_pies:pumpkin_pie_mix",
    recipe = {
        {"gpumpkin_pies:applesauce", "group:food_sugar", "group:food_milk"},
        {"pumpkin_pies:pumpkin_puree", "pumpkin_pies:pumpkin_puree", "pumpkin_pies:pumpkin_puree"},
        {"pumpkin_pies:pumpkin_puree", "group:food_mixing_bowl", "pumpkin_pies:pumpkin_puree"}
    },
    replacements = {
        {"group:food_mixing_bowl", "farming:mixing_bowl"},
        {"pumpkin_pies:applesauce", "vessels:drinking_glass"},
        {"mobs:bucket_milk", "bucket:bucket_empty"},
        {"mobs:wooden_bucket_milk", "wooden_bucket:wooden_bucket_empty"},
        {"animalia:bucket_milk", "bucket:bucket_empty"}
    }
})

-- Pumpkin Pie Mix Alternative 2
minetest.register_craftitem("pumpkin_pies:pumpkin_pie_mix", {
    description = "Pumpkin Pie Mix",
    inventory_image = "pumpkin_pie_mix.png",
    on_use = minetest.item_eat(6)
})

minetest.register_craft({
    output = "pumpkin_pies:pumpkin_pie_mix",
    recipe = {
        {"pumpkin_pies:applesauce", "group:food_sugar", "farming:soy_milk"},
        {"pumpkin_pies:pumpkin_puree", "pumpkin_pies:pumpkin_puree", "pumpkin_pies:pumpkin_puree"},
        {"pumpkin_pies:pumpkin_puree", "group:food_mixing_bowl", "pumpkin_pies:pumpkin_puree"}
    },
    replacements = {
        {"group:food_mixing_bowl", "farming:mixing_bowl"},
        {"pumpkin_pies:applesauce", "vessels:drinking_glass"},
        {"farming:mixing_bowl", "vessels:drinking_glass"},
        {"farming:soy_milk", "vessels:drinking_glass"}
    }
})

-- Pumpkin Pie Mix Alternative 3
minetest.register_craftitem("pumpkin_pies:pumpkin_pie_mix", {
    description = "Pumpkin Pie Mix",
    inventory_image = "pumpkin_pie_mix.png",
    on_use = minetest.item_eat(6)
})

minetest.register_craft({
    output = "pumpkin_pies:pumpkin_pie_mix",
    recipe = {
        {"group:food_egg", "group:food_sugar", "farming:soy_milk"},
        {"pumpkin_pies:pumpkin_puree", "pumpkin_pies:pumpkin_puree", "pumpkin_pies:pumpkin_puree"},
        {"pumpkin_pies:pumpkin_puree", "group:food_mixing_bowl", "pumpkin_pies:pumpkin_puree"}
    },
    replacements = {
        {"group:food_mixing_bowl", "farming:mixing_bowl"},
        {"farming:mixing_bowl", "vessels:drinking_glass"},
        {"farming:soy_milk", "vessels:drinking_glass"}
    }
})

-- Raw Pumpkin Pie
minetest.register_craftitem("pumpkin_pies:raw_pumpkin_pie", {
    description = "Raw Pumpkin Pie",
    inventory_image = "pumpkin_pie_raw.png",
    on_use = minetest.item_eat(10)
})

minetest.register_craft({
    output = "pumpkin_pies:raw_pumpkin_pie",
    recipe = {
        {"farming:flour", "pumpkin_pies:pumpkin_pie_mix", "farming:flour"},
        {"", "farming:flour", ""},
        {"", "farming:baking_tray", ""}
    },
    replacements = {{"group:food_baking_tray", "farming:baking_tray"}}
})

-- Gourmet Pumpkin Pie
minetest.register_craftitem("pumpkin_pies:gourmet_pumpkin_pie", {
    description = "Gourmet Pumpkin Pie",
    inventory_image = "pumpkin_pie_gourmet.png",
    on_use = minetest.item_eat(15)
})

minetest.register_craft({
    type = "cooking",
    output = "pumpkin_pies:gourmet_pumpkin_pie",
    recipe = "pumpkin_pies:raw_pumpkin_pie",
    cooktime = 10
})

-- Pumpkin Smoothie
minetest.register_craftitem("pumpkin_pies:pumpkin_smoothie", {
    description = "Pumpkin Smoothie",
    inventory_image = "pumpkin_smoothie.png",
    on_use = minetest.item_eat(5, "vessels:drinking_glass"),
    groups = {vessel = 1, drink = 1}
})

minetest.register_craft ({
    output = "pumpkin_pies:pumpkin_smoothie",
    recipe = {
        {"group:food_milk", "pumpkin_pies:pumpkin_puree", "group:food_sugar"},
        {"", "default:snow", ""},
        {"", "vessels:drinking_glass", ""}
    },
    replacements = {
        {"mobs:bucket_milk", "bucket:bucket_empty"},
        {"mobs:wooden_bucket_milk", "wooden_bucket:wooden_bucket_empty"},
        {"animalia:bucket_milk", "bucket:bucket_empty"}
    }
})

-- Pumpkin Smoothie Alternative
minetest.register_craftitem("pumpkin_pies:pumpkin_smoothie", {
    description = "Pumpkin Smoothie",
    inventory_image = "pumpkin_smoothie.png",
    on_use = minetest.item_eat(5, "vessels:drinking_glass"),
    groups = {vessel = 1, drink = 1, compostability = 65}
})

minetest.register_craft ({
    output = "pumpkin_pies:pumpkin_smoothie",
    recipe = {
        {"farming:soy_milk", "pumpkin_pies:pumpkin_puree", "group:food_sugar"},
        {"", "default:snow", ""},
        {"", "vessels:drinking_glass", ""}
    },
    replacements = {
        {"farming:soy_milk", "vessels:drinking_glass"}
    }
})
