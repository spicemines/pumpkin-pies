# Pumpkin Pies

This is a mod for Minetest that adds various recipes for making pumpkin pies. You can start off with some pumpkin puree for a quick snack, or craft a gourmet pie for some real sustenance.

This mod requires the farming mod by TenPlus1 for the various crafting recipes. Optionally, the mobs mod by TenPlus1 can be added for eggs and milk. Also, the eggs and bucket of milk from the Animalia mod by ElCeejo will work with the pumpkin pie mix recipe.

## Applesauce

Though you may not think applesauce to be an ingredient for pumpkin pies, it is an egg substitute. So, this mod adds applesauce as an alternative ingredient for the pumpkin pie mix.

## Pumpkin Puree

Quick to craft. Quick to eat. Mash up a pumpkin slice with a mortar and pestle and you'll have yourself some pumpkin puree.

## Pumpkin Pie

This basic pie was inspired by the rhubarb pie from the farming mod. In fact, the recipe only differs in the ingredients.

## Cooked Pumpkin Pie

Place your basic pumpkin pies in the furnace to make cooked pumpkin pies. These will restore a little more of your hunger bar.

## Gourmet Pumpkin Pie

Well worth the wait. Take the time to craft a gourmet pumpkin pie and you'll be rewarded with great tasting sustenance.

## Pumpkin Smoothie

New for the Fall 2023 season! Mix up and enjoy a hearty pumpkin smoothie.

### The Recipe

**Items needed**

- Mortar and pestle
- Mixing bowl
- Baking tray
- Fueled furnace

**Ingredients**

- Pumpkin slices
- Egg, or applesauce
- Sugar
- Bucket of milk, or soy milk

**Procedure**

Start off by crafting some pumpkin puree. Then, combine five pumpkin purees, one egg or applesauce, one sugar, one bucket of milk or soy milk, and a mixing bowl in the crafting grid to make pumpkin pie mix. Next, combine the pumpkin pie mix with flour and a baking tray in the crafting grid to make a raw pumpkin pie. Bake the raw pumpkin pie (or a stack of them) in the furnace to produce gourmet pumpkin pies.

---
## Crafting Guide

### Pumpkim Puree

This is a shapeless recipe. Simply add any number of pumpkin slices to a single spot on the crafting grid and add in a mortar and pestle.

### Applesauce (used as a snack or as an egg alternative)

```
-———————————-   
|   | A |   |   
|---+---+---|   
|   | M |   |   
|---+---+---|   
|   | G |   |   
-———————————-
```

A = Apple   
M = Mortar and Pestle   
G = Drinking Glass

### Pumpkin Pie (Basic)

```
-———————————-   
| B | S | - |   
|---+---+---|   
| P | P | P |   
|---+---+---|   
| W | W | W |   
-———————————-
```

B = Baking tray   
S = Sugar   
P = Pumpkin Puree   
W = Wheat

### Pumpkin Pie Mix

```
-———————————-   
| E | S | M |   
|---+---+---|   
| P | P | P |   
|---+---+---|   
| P | B | P |   
-———————————-
```

B = Mixing bowl   
E = Egg   
M = Bucket of Milk   
P = Pumpkin puree   
S = Sugar

### Pumpkin Pie Mix (Alternative)

```
-———————————-   
| A | S | M |   
|---+---+---|   
| P | P | P |   
|---+---+---|   
| P | B | P |   
-———————————-
```

B = Mixing bowl   
A = Applesauce   
M = Soy Milk   
P = Pumpkin puree   
S = Sugar

### Raw Pumpkin Pie

```
-———————————-   
| F | M | F |   
|---+---+---|   
| - | F | - |   
|---+---+---|   
| - | B | - |   
-———————————-
```

B = Baking tray   
F = Flour   
M = Pumpkin pie mix   

### Pumpkin Smoothie

```
-———————————-
| M | P | S |
|---+---+---|
| - | I | - |
|---+---+---|
| - | G | - |
-———————————-
```

M = Milk or Soy Milk   
P = Pumpkin Puree   
S = Sugar   
I = Snowball (Ice)   
G = Drinking Glass

## Credits

The textures used for the applesauce, basic pumpkin pie, cooked pumpkin pie, pumpkin puree, and pumpkin pie mix were all adapted from textures in the Farming mod by TenPlus1.

## Licensing

All source code in this mod is licensed under the Apache 2.0 license.

All textures are licensed under the CC-BY 3.0 license.
